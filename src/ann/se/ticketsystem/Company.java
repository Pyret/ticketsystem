package ann.se.ticketsystem;

public class Company {

	String myName;
	int myEmployees;
	int myAirplanes;
	String myPrefix;
	String create_date;
	int company_id;
	
	public Company() {
		// TODO Auto-generated constructor stub
	}


	public Company(int id, String name, int employees, int airplanes, String prefix, String created){
		this.myName = name;
		this.myEmployees = employees;
		this.myAirplanes = airplanes;
		this.myPrefix= prefix;
		this.company_id = id;
		this.create_date = created;
		
	}


	public String getMyName() {
		return myName;
	}


	public void setMyName(String myName) {
		this.myName = myName;
	}


	public int getMyEmployees() {
		return myEmployees;
	}


	public void setMyEmployees(int myEmployees) {
		this.myEmployees = myEmployees;
	}


	public int getMyAirplanes() {
		return myAirplanes;
	}


	public void setMyAirplanes(int myAirplanes) {
		this.myAirplanes = myAirplanes;
	}

	public String getMyPrefix() {
		return myPrefix;
	}


	public void setMyPrefix(String myPrefix) {
		this.myPrefix = myPrefix;
	}


	public String getCreate_date() {
		return create_date;
	}


	public void setCreate_date(String create_date) {
		this.create_date = create_date;
	}


	public int getCompany_id() {
		return company_id;
	}


	public void setCompany_id(int company_id) {
		this.company_id = company_id;
	}




}

package ann.se.ticketsystem;

import java.sql.Connection;
import java.util.Scanner;

public class Menu {

	static String regexSvarPrintPattern = "^[12345]{1,1}$"; // Validerar
	// printmenyn
	static String regexSvarPattern = "^[1234]{1,1}$"; // Validerar huvudmeny
	static String regexSvarAdminPattern = "^[1234567]{1,1}$"; // Validerar
	// adminmenyn

	static Connection myConn = null;

	public Menu() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * Huvudmenyn
	 */
	public static void mainMenu() {
		String answer;
		Scanner user_input = new Scanner(System.in);
		Admin admin = new Admin();

		System.out.println("\n---------------------------------");
		System.out.println("Huvudmeny:");
		System.out.println("---------------------------------");
		System.out.println("1. Sök/boka resa");
		System.out.println("2. Se mina bokningar");
		System.out.println("3. Admin");
		System.out.println("4. Avsluta");
		System.out.println("---------------------------------");
		System.out.println("");

		System.out.print("Vad vill du göra: ");

		do {
			answer = user_input.next();
			if (answer.matches(regexSvarPattern)) {
				break;
			} else {
				System.out
				.print("\nSkriv in ditt menyval korrekt, ange en siffra! ");
			}

		} while (!answer.matches(regexSvarPattern));

		switch (answer) {

		case "1":
			admin.SearchBookingTicket(user_input);
			break;

		case "2":
			admin.PrintMyTicket(user_input);
			break;

		case "3":
			menuAdmin();

		case "4":
			System.exit(0);
			break;

		default:
			break;
		}

	}

	/**
	 * Adminmenyn
	 */
	public static void menuAdmin() {
		String answerAdmin;
		Scanner user_input = new Scanner(System.in);
		Admin admin = new Admin();

		System.out.println("\n---------------------------------");
		System.out.println("Adminmeny:");
		System.out.println("---------------------------------");
		System.out.println("1. Skapa flygplats");
		System.out.println("2. Skapa flygbolag");
		System.out.println("3. Skapa flyglinje");
		System.out.println("4. Utskrifter");
		System.out.println("5. Skapa databas och tabeller");
		System.out.println("6. Avsluta, åter till huvudmenyn");
		System.out.println("---------------------------------\n");

		System.out.print("Vad vill du göra: ");

		do {
			answerAdmin = user_input.next();
			if (answerAdmin.matches(regexSvarAdminPattern)) {
				break;
			} else {
				System.out
				.print("\nSkriv in ditt menyval korrekt, ange en siffra: ");
			}

		} while (!answerAdmin.matches(regexSvarAdminPattern));

		outerloop:
		switch (answerAdmin) {
		case "1":
			admin.createNewAirport(user_input);
			break;

		case "2":
			admin.createNewCompany(user_input);
			break;

		case "3":
			admin.createNewFlightLine(user_input);
			break;

		case "4":
			menuPrint();
			break;

		case "5":
			System.out.println("\nVill du verkligen radera databasen och skapa en ny:");
			System.out.println("1. Ja");
			System.out.println("2. Avsluta, åter till adminmenyn");

			int svar = user_input.nextInt();

			do {
				if (svar == 1) {	

					ConnectionMySQL.deleteMysql();
					ConnectionMySQL.createMysql();
					ConnectionMySQL.insertValues();
					System.out
					.println("\nNedanstående databas och tabeller är nu skapade med defaultvärden.");
					System.out.println("\nDatabasen: \tTicketsystem");
					System.out.println("Tabellerna:\tAirport");
					System.out.println("\t\tCompany");
					System.out.println("\t\tFlightline");
					System.out.println("\t\tPassenger");
					System.out.println("\t\tTicket");
				}
				if (svar == 2) {
					menuAdmin();	
				}
			} while (false);
			break;

		case "6":
			mainMenu();
			break;

		default:
			break outerloop;
		}
	}

	/**
	 * Undermenyn (4. Utskrifter) till Adminmenyn
	 */
	public static void menuPrint() {

		String answerPrint;
		Scanner user_input = new Scanner(System.in);
		Admin admin = new Admin();
		System.out.println("\n---------------------------------");
		System.out.println("Utskriftsmeny: ");
		System.out.println("---------------------------------");
		System.out.println("1. Alla bokningar");
		System.out.println("2. Alla flygplatser");
		System.out.println("3. Alla flygbolag");
		System.out.println("4. Alla flyglinjer");
		System.out.println("5. Avsluta, åter till adminmeny");
		System.out.println("---------------------------------\n");

		System.out.print("Vad vill du skriva ut: ");

		do {
			answerPrint = user_input.next();

			if (answerPrint.matches(regexSvarPrintPattern)) {
				break;
			} else {
				System.out
				.print("\nSkriv in ditt menyval korrekt, ange en siffra: ");
			}

		} while (!answerPrint.matches(regexSvarPrintPattern));

		switch (answerPrint) {
		case "1":
			System.out.println("\nAlla bokningar");
			admin.PrintAllTicket(user_input);
			break;

		case "2":
			System.out.println("\nAlla flygplatser");
			ConnectionMySQL.getAirport(Queries.SHOW_AIRPORTS);
			break;

		case "3":
			System.out.println("\nAlla flygbolag");
			ConnectionMySQL.getCompany(Queries.SHOW_COMPANY);
			break;

		case "4":
			System.out.println("\nAlla flyglinjer");
			ConnectionMySQL.getFlightline(Queries.SHOW_ALL_FLIGHTLINES);
			break;

		case "5":
			menuAdmin();
			break;

		default:
			break;
		}
	}
}
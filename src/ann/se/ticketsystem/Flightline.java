package ann.se.ticketsystem;

public class Flightline {
	String myName;
	String myArrAirport;
	String myDepAirport;
	int myFlightline_id;
	String create_date;
	int flightline_id;
	String arr_date;





	public Flightline(String name,String arr_date,String arrAirport, String depAirport, int flightline_id, String create_date){
		this.myName = name;
		this.myArrAirport = arrAirport;
		this.myDepAirport = depAirport;
		this.myFlightline_id = flightline_id;
		this.create_date = create_date;
		this.flightline_id= 0;
		this.arr_date=arr_date;
	}



	public Flightline(int flightline_id, String arr_date,String name, String arrAirport,
			String depAirport, String create_date) {
		this.myName = name;
		this.myArrAirport = arrAirport;
		this.myDepAirport = depAirport;
		this.myFlightline_id = flightline_id;
		this.create_date = create_date;
		this.flightline_id= flightline_id;
		this.arr_date=arr_date;
	}



	public String getMyName() {
		return myName;
	}



	public void setMyName(String myName) {
		this.myName = myName;
	}



	public String getMyArrAirport() {
		return myArrAirport;
	}



	public void setMyArrAirport(String myArrAirport) {
		this.myArrAirport = myArrAirport;
	}



	public String getMyDepAirport() {
		return myDepAirport;
	}



	public void setMyDepAirport(String myDepAirport) {
		this.myDepAirport = myDepAirport;
	}


	public int getMyFlightline_id() {
		return myFlightline_id;
	}



	public void setMyFlightline_id(int myFlightline_id) {
		this.myFlightline_id = myFlightline_id;
	}



	public String getCreate_date() {
		return create_date;
	}



	public void setCreate_date(String create_date) {
		this.create_date = create_date;
	}



	public String getArr_date() {
		return arr_date;
	}



	public void setArr_date(String arr_date) {
		this.arr_date = arr_date;
	}

	public int getFlightline_id() {
		return flightline_id;
	}



	public void setFlightline_id(int flightline_id) {
		this.flightline_id = flightline_id;
	}

	public String showAll(){
		return this.myFlightline_id + "\t" + this.myName + "\t" + this.myArrAirport + "\t" + this.myDepAirport  + "\t" +this.create_date;
	}

	public void printAll(){
		System.out.println(this.myFlightline_id + "\t" + this.myName + "\t" + this.myArrAirport + "\t" + this.myDepAirport  + "\t" +this.create_date);
	}



}

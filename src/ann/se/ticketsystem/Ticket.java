package ann.se.ticketsystem;

import java.util.Date;
import java.util.Formatter;

/**
 * @author Ann
 *
 */
public class Ticket {

	int myTicket_id;
	int myFlightline_id;
	String myArr_date;
	String myArrAirport;
	String myDepAirport;
	String myFlightline_name;
	String myCompany_name;
	int myPassenger_id;
	String myFirstName;
	String myLastName;
	String myBookingnumber;
	String create_date;

	public Ticket(int ticket_id, int flightline_id, String arr_date,
			String arrAirport, String depAirport, String flightline_name,
			String company_name, int passenger_id, String firstName,
			String lastName, String bookingnumber, String create_date){

		this.myTicket_id = ticket_id;
		this.myFlightline_id = flightline_id;
		this.myArr_date = arr_date;
		this.myArrAirport = arrAirport;
		this.myDepAirport = depAirport;
		this.myFlightline_name = flightline_name;
		this.myCompany_name = company_name;
		this.myPassenger_id = passenger_id;
		this.myFirstName = firstName;
		this.myLastName = lastName;
		this.myBookingnumber = bookingnumber;
		this.create_date = create_date;
	}



	public Ticket() {
		// TODO Auto-generated constructor stub
	}


	public Ticket(int ticket_id, String arr_date, String arrAirport,
			String depAirport, String flightline_name, String company_name,
			String firstName, String lastName, String bookingnumber,
			String create_date2) {
		// TODO Auto-generated constructor stub
	}


	public int getMyTicket_id() {
		return myTicket_id;
	}



	public void setMyTicket_id(int myTicket_id) {
		this.myTicket_id = myTicket_id;
	}



	public String getCreate_date() {
		return create_date;
	}



	public void setCreate_date(String create_date) {
		this.create_date = create_date;
	}



	public int getMyPassenger_id() {
		return myPassenger_id;
	}



	public void setMyPassenger_id(int myPassenger_id) {
		this.myPassenger_id = myPassenger_id;
	}



	public int getMyFlightline_id() {
		return myFlightline_id;
	}



	public void setMyFlightline_id(int myFlightline_id) {
		this.myFlightline_id = myFlightline_id;
	}



	public String getMyBookingnumber() {
		return myBookingnumber;
	}



	public void setdepAirport(String depAirport) {
		// TODO Auto-generated method stub

	}



	public void setMyBookingnumber(String phonenumber) {
		// TODO Auto-generated method stub

	}

	public void printAll() {
		System.out.println(this.myTicket_id + "\t" + this.myArr_date + "\t"
				+ this.myArrAirport + "\t" + this.myDepAirport + "\t"
				+ this.myFlightline_name + "\t" + this.myCompany_name + "\t"
				+ this.myFirstName + "\t" + this.myLastName + "\t"
				+ this.myBookingnumber + "\t" + this.create_date);

	}
//	public void printPassenger(){
//		System.out
//		.println("\n-------------------------------------------");
//		System.out.println("\nNAMN:");
//		System.out.println(this.myFirstName +  " "
//				+  this.myLastName + "\n");
//		System.out
//		.println("-------------------------------------------");
//		System.out.println("\n\n\n");
//		Formatter f = new Formatter();
//		System.out.println(f.format("%-10s %-5s %-5s %-10s",
//				"DATUM", "FRÅN", "TILL", "FLYGNR"));
//		Formatter g = new Formatter();
//		System.out.println(g.format("%-10s %-5s %-5s %-10s",
//				this.myArr_date, this.myArrAirport,
//				this.myDepAirport, this.myCompany_name));
//		System.out
//		.println("\n\n\n-------------------------------------------");
//	}
}
